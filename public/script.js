window.addEventListener('load', () => {
    // Client-side form validation by Bootstrap
    const forms = document.querySelectorAll('form');

    for (const form of forms) {
        form.addEventListener('submit', event => {
            if (!form.checkValidity()) {
                event.preventDefault();
            }

            form.classList.add('was-validated');
        });
    }

    // Toggle pills by anchor
    if (document.querySelectorAll('.nav-pills').length > 0 && window.location.hash) {
        document.querySelector('[href="' + window.location.hash + '"]').click();
    }

    // Todo markers
    document.querySelectorAll('[data-todo]').forEach(elem => {
        elem.addEventListener('click', e => {
            e.preventDefault();
            alert('Funktion nicht implementiert');
        })
    });

    // IBAN validator
    document.querySelectorAll('[data-validate*="iban"]').forEach(elem => {
        elem.addEventListener('change', () => {
            const http = new XMLHttpRequest();

            http.onreadystatechange = () => {
                if (http.readyState == 4 && http.status == 200) {
                    const response = JSON.parse(http.responseText);
                    elem.classList.toggle('is-valid', response.valid);
                    elem.classList.toggle('is-invalid', !response.valid);
                }
            };

            http.open('GET', '/validate?type=iban&value=' + elem.value, true);
            http.send();
        });
    });
});