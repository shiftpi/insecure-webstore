<?php
declare(strict_types = 1);

namespace InsecureWebstore;

// Don't handle static file requests
if (php_sapi_name() === 'cli-server'
    && is_readable(__DIR__  . $_SERVER['REQUEST_URI'])
    && is_file(__DIR__  . $_SERVER['REQUEST_URI'])
) {
    return false;
}

session_start();

// Insecure: Will show all errors and warnings
ini_set('display_errors', '1');
error_reporting(E_ALL);

// Make applications root to working directory
chdir(__DIR__ . '/../');

// PSR-4 autoloader
spl_autoload_register(function($className) {
    if (strpos($className, 'InsecureWebstore\\') !== 0) {
        return false;
    }

    $className = str_replace('InsecureWebstore\\', '', $className);
    $path = 'src/' . str_replace('\\', '/', $className) . '.php';

    if (is_readable($path)) {
        require $path;
        return true;
    }

    return false;
});

(new \InsecureWebstore\Tool\Router())
    ->dispatch($_SERVER['REQUEST_URI'])
    ->run();