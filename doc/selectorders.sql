USE webstore;

SELECT o.id orderid, c.email customer, ao.amount, a.title article
  FROM `order`o
  INNER JOIN customer c on o.customer_id = c.id
  INNER JOIN article_order ao on o.id = ao.order_id
  INNER JOIN article a on ao.article_id = a.id
  ORDER BY orderid;