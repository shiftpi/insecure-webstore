const gulp = require('gulp'),
      sass = require('gulp-sass');

gulp.task('watch', () => {
    gulp.watch('./*.scss', ['build-css']);
});

gulp.task('build-css', () => {
    return gulp.src('./style.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest('../public'));
});

gulp.task('default', ['build-css']);