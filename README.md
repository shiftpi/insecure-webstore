# Insecure web store

## Anforderungen
* PHP >=7.1
* MySQL >= 5.7
* Node.js with NPM (nur zum Builden des Frontends)

## DB erzeugen
Datenbank erzeugen:  ```doc/createdb.sql```
Demodaten: ```doc/demodata.sql```

### Verbindung konfigurieren
```config/local.php.dist``` nach ```config/local.php``` kopieren und Parameter enstprechend der lokalen Umgebung anpassen.

## Frontend assets
```npm i``` in ```frontend/``` ausführen, um Gulp und seine Abhängigkeiten zu installieren.
```gulp``` in ```frontend/``` ausführen, um die Frontend-Assets zu bauen oder ```gulp watch``` um Gulp im Watch-Modus zu starten.

## Webserver starten
```bash
php -S localhost:8080 -t public public/index.php
```

### php.ini
Folgende Einstellungen müssen in der ```php.ini``` geändert werden, ausgehend von einer Default-Konfiguration:
```
session.cookie_httponly = Off
allow_url_fopen = On
```

## Anmeldedaten
E-Mail: ```janedoe@example.com``` 
Passwort: ```foobar```

E-Mail: ```johndoe@example.com```
Passwort: ```foobar```

## Bilder
Alle Bilder verwendeten Bilder stammen von [Pexels](https://www.pexels.com/) und sind unter der [Creative Commons Zero (CC0) license](https://www.pexels.com/photo-license/) lizenziert.

## Sicherheitslücken
### Controller
* Article controller: SQL Injection
* Cart controller: CSRF
* Category controller: Ungefilterter Input (wird später abgefangen)
* Checkout controller: Zu langer Input für DB (wird abgefangen)
* Login controller: Open redirect
* Logout controller: CSRF
* Review controller: Keine Berechtigungsprüfung, zu langer Input für DB (wird abgefangen), ungefilterter Input (XSS)

### Mapper
* Article mapper: SQL Injection
* Customer mapper: MD5 für Passwort-Hashing (ungesalzen und angreifbar)

### Tool
* Auth tool: User-Objekt wird aus Session ausgelesen => Daten können in DB verändert werden, ohne dass es sich auf angemeldete Nutzer auswirkt
* Db connection tool: Information leak => über Suchfunktion

### public-Ordner
* /phpinfo.php: Information leak