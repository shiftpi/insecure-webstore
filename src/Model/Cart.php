<?php
declare(strict_types=1);

namespace InsecureWebstore\Model;

use InsecureWebstore\Mapper\Article as ArticleMapper;

class Cart
{
    /** @var Cart */
    private static $instance;

    public function __construct()
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = [];
        }
    }

    public static function getInstance(): Cart
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function addArticle(int $amount, Article $article)
    {
        if (isset($this->session[$article->getId()])) {
            $_SESSION['cart'][$article->getId()] += $amount;
        } else {
            $_SESSION['cart'][$article->getId()] = $amount;
        }
    }

    public function removeArticle(int $articleId)
    {
        unset($_SESSION['cart'][$articleId]);
    }

    public function clear()
    {
        unset($_SESSION['cart']);
    }

    /**
     * @return CartEntry[]
     */
    public function getArticles(): array
    {
        $mapper = new ArticleMapper();

        $cart = [];

        foreach ($_SESSION['cart'] as $articleId => $amount) {
            $article = $mapper->findById((int) $articleId);

            if (!$article) {
                continue;
            }

            $cart[] = new CartEntry($amount, $article);
        }

        return $cart;
    }
}