<?php
declare(strict_types=1);

namespace InsecureWebstore\Model;

class CartEntry
{
    /** @var int */
    private $amount;

    /** @var Article */
    private $article;

    /** @var string */
    private $review;

    /**
     * CartEntry constructor.
     * @param int $amount
     * @param Article $article
     * @param string $review (optional)
     */
    public function __construct(int $amount, Article $article, string $review = null)
    {
        $this->setAmount($amount);
        $this->setArticle($article);

        if ($review) {
            $this->setReview($review);
        }
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }

    /**
     * @param Article $article
     */
    public function setArticle(Article $article): void
    {
        $this->article = $article;
    }

    /**
     * @return string|null
     */
    public function getReview(): ?string
    {
        return $this->review;
    }

    /**
     * @param string $review
     */
    public function setReview(string $review): void
    {
        $this->review = $review;
    }

    public function getCost(): float
    {
        return $this->getAmount() * $this->getArticle()->getPrice();
    }
}