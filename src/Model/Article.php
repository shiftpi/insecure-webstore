<?php
declare(strict_types=1);

namespace InsecureWebstore\Model;

class Article
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var float */
    private $price;

    /** @var string */
    private $image;

    /** @var string[] */
    private $reviews;

    /**
     * Article constructor.
     * @param int $id (optional)
     * @param string $title (optional)
     * @param string $description (optional)
     * @param float $price (optional)
     * @param string $image (optional)
     * @param string[] $reviews (optional)
     */
    public function __construct(int $id = null, string $title = null, string $description = null, float $price = null,
        string $image = null, array $reviews = null)
    {
        if ($id) {
            $this->setId($id);
        }

        if ($title) {
            $this->setTitle($title);
        }

        if ($description) {
            $this->setDescription($description);
        }

        if ($price) {
            $this->setPrice($price);
        }

        if ($image) {
            $this->setImage($image);
        }

        if ($reviews) {
            $this->setReviews($reviews);
        }
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = (int) $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float) $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price): void
    {
        $this->price = (float) $price;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return string[]
     * @throws \Exception
     */
    public function getReviews(): array
    {
        if (!is_array($this->reviews)) {
            throw new \Exception('Reviews have not been requested from database');
        }

        return $this->reviews;
    }

    /**
     * @param string[] $reviews
     */
    public function setReviews(array $reviews)
    {
        $this->reviews = $reviews;
    }

    public function addReview(string $review)
    {
        $this->reviews[] = $review;
    }

    public static function factory(array $options = [])
    {
        $instance = new static();

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (method_exists($instance, $method)) {
                $instance->$method($value);
            }
        }

        return $instance;
    }
}