<?php
declare(strict_types=1);

namespace InsecureWebstore\Model;

class Order
{
    /** @var int */
    private $id;

    /** @var \DateTime */
    private $created;

    /** @var int */
    private $customerId;

    /** @var string */
    private $iban;

    /** @var string */
    private $comment;

    /** @var CartEntry[] */
    private $cartEntries = [];

    /**
     * Order constructor
     * @param int $id (optional)
     * @param \DateTime|string $created (optional)
     * @param string $iban (optional)
     * @param string $comment (optional)
     * @param int $customerId (optional)
     */
    public function __construct(int $id = null, $created = null, string $iban = null, string $comment = null,
        int $customerId = null)
    {
        if ($id) {
            $this->setId($id);
        }

        if ($created) {
            $this->setCreated($created);
        }

        if ($iban) {
            $this->setIban($iban);
        }

        if ($comment) {
            $this->setComment($comment);
        }

        if ($customerId) {
            $this->setCustomerId($customerId);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime|string $created
     */
    public function setCreated($created): void
    {
        if (!$created instanceof \DateTime) {
            $created = \DateTime::createFromFormat('Y-m-d H:i:s', $created);
        }

        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban(string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId(int $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return CartEntry[]
     */
    public function getCartEntries(): array
    {
        return $this->cartEntries;
    }

    /**
     * @param CartEntry[] $cartEntries
     */
    public function setCartEntries(array $cartEntries): void
    {
        $this->cartEntries = $cartEntries;
    }

    public function addCartEntry(CartEntry $cartEntries)
    {
        $this->cartEntries[] = $cartEntries;
    }
}