<?php
declare(strict_types=1);

namespace InsecureWebstore\Validator;

interface ValidatorInterface
{
    public function isValid(string $value);
}