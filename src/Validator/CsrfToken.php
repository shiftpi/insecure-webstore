<?php
declare(strict_types=1);

namespace InsecureWebstore\Validator;

class CsrfToken implements ValidatorInterface
{
    public function getToken(): string
    {
        if (!isset($_SESSION['csrf'])) {
            $_SESSION['csrf'] = hash('sha3-256', random_bytes(48));
        }

        return $_SESSION['csrf'];
    }

    public function isValid(string $token): bool
    {
        // hash_equals protects against timing attacks
        $success = isset($_SESSION['csrf']) && hash_equals($_SESSION['csrf'], $token);

        if ($success) {
            unset($_SESSION['csrf']);
        }

        return $success;
    }
}