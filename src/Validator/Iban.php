<?php
declare(strict_types=1);

namespace InsecureWebstore\Validator;

class Iban implements ValidatorInterface
{
    public function isValid(string $iban)
    {
        try {
            // File get contents allows URLs (potentially insecure)
            $result = @json_decode(file_get_contents('https://openiban.com/validate/' . $iban));
        } catch (\Throwable $e) {
            return false;                                                       // Request failed
        }

        return isset($result->valid) && $result->valid ? $iban : false;
    }
}