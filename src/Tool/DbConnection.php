<?php
declare(strict_types=1);

namespace InsecureWebstore\Tool;

class DbConnection
{
    /** @var DbConnection */
    private static $instance;

    /** @var \PDO */
    private $connection;

    public static function getInstance(): DbConnection
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {
        $config = Config::getInstance()->getLocal()['db'];

        $this->connection = new \PDO(sprintf('mysql:host=%s;dbname=webstore;charset=UTF8', $config['host']),
            $config['username'], $config['password']);

        // Potential information leak if exception does not caught probably
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function getConnection(): \PDO
    {
        return $this->connection;
    }
}