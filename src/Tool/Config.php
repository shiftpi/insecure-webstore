<?php
declare(strict_types=1);

namespace InsecureWebstore\Tool;

class Config
{
    /** @var Config */
    private static $instance;

    public static function getInstance(): Config
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getLocal(): array
    {
        $path = 'config/local.php';

        if (!is_readable($path)) {
            throw new \Exception('No config file found');
        }

        return require $path;
    }
}