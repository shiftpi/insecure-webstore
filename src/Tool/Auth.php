<?php
declare(strict_types=1);

namespace InsecureWebstore\Tool;

use InsecureWebstore\Mapper\Customer as CustomerMapper;
use InsecureWebstore\Model\Customer;

class Auth
{
    private static $instance;

    public static function getInstance(): Auth
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function authenticate(string $email, string $password): bool
    {
        $user = (new CustomerMapper())->findByEmailPassword($email, $password);

        if ($user) {
            $_SESSION['user'] = serialize($user);
        }

        return !!$user;
    }

    public function unAuthenticate()
    {
        unset($_SESSION['user']);
    }

    public function getUser(): ?Customer
    {
        // Potentially inconsistent data in a critical context: User can be changed in database while user in
        // session is still the old one
        return isset($_SESSION['user']) ? unserialize($_SESSION['user'], [Customer::class]) : null;
    }
}