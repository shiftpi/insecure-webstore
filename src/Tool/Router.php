<?php
declare(strict_types=1);

namespace InsecureWebstore\Tool;

use InsecureWebstore\Controller;

class Router
{
    private $routes = [
        '' => Controller\Homepage::class,
        'homepage' => Controller\Homepage::class,
        'article' => Controller\Article::class,
        'cart' => Controller\Cart::class,
        'category' => Controller\Category::class,
        'checkout' => Controller\Checkout::class,
        'login' => Controller\Login::class,
        'logout' => Controller\Logout::class,
        'profile' => Controller\Profile::class,
        'review' => Controller\Review::class,
        'search' => Controller\Search::class,
        'validate' => Controller\Validator::class,
    ];

    public function dispatch(string $url): Controller\AbstractController
    {
        // First segment of URL path is our controller key
        $segments = explode('/', $url);
        $controller = count($segments) > 0 ? $segments[1] : '';

        if ($pos = strpos($controller, '?')) {
            $controller = substr($controller, 0, $pos);
        }

        // Map controller key to controller class
        $controller = $this->routes[$controller] ?? Controller\NotFound::class;

        return new $controller();
    }
}