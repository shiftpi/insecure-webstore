<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Model\Order;
use InsecureWebstore\Tool\Auth;
use InsecureWebstore\Validator;

class Checkout extends AbstractController
{
    protected $title = 'Bestellung abschließen';

    public function run()
    {
        if (!$customer = Auth::getInstance()->getUser()) {
            return $this->render('checkout_login');
        }

        $errors = [];
        $csrf = new Validator\CsrfToken();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = filter_input_array(INPUT_POST, [
                'iban' => [
                    'filter' => FILTER_CALLBACK,
                    'options' => [
                        new Validator\Iban, 'isValid'
                    ],
                ],
                'comment' => [
                    'filter' => FILTER_SANITIZE_STRING,
                ],
                'csrf' => [
                    'filter' => FILTER_CALLBACK,
                    'options' => [
                        $csrf, 'isValid'
                    ],
                ],
            ]);

            foreach ($data as $field => $value) {
                if ($value === false) {
                    $errors[$field] = true;
                }
            }

            if (count($errors) === 0) {
                $order = new Order(
                    null,
                    null,
                    $data['iban'],
                    substr($data['comment'], 0, 65535),                     // Server side protection against too long inputs
                    $customer->getId()
                );

                $cart = new \InsecureWebstore\Model\Cart();
                $order->setCartEntries($cart->getArticles());

                (new \InsecureWebstore\Mapper\Order())->save($order);
                $cart->clear();

                return $this->render('checkout_thanks');
            }
        }

        $this->render('checkout', [
            'customer' => $customer,
            'errors' => $errors,
            'csrf' => $csrf->getToken(),
        ]);
    }
}