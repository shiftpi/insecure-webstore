<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Order as OrderMapper;
use InsecureWebstore\Validator\CsrfToken;

class Review extends AbstractController
{
    protected $title = 'Artikel bewerten';

    public function run()
    {
        $errors = [];

        $getData = filter_input_array(INPUT_GET, [
            'article' => [
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_REQUIRE_SCALAR,
            ],
            'order' => [
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_REQUIRE_SCALAR,
            ],
        ]);

        if (!$getData['article'] || !$getData['order']) {
            return $this->notFound();
        }

        $csrf = new CsrfToken();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Permission check missing
            $postData = filter_input_array(INPUT_POST, [
                'review' => [
                    // Filter not set => XSS possible
                    'flags' => FILTER_REQUIRE_SCALAR,
                ],
                'csrf' => [
                    'filter' => FILTER_CALLBACK,
                    'options' => [
                        $csrf, 'isValid',
                    ],
                ],
            ]);

            foreach ($postData as $field => $value) {
                if ($value === false) {
                    $errors[$field] = true;
                }
            }

            if (count($errors) === 0) {
                (new OrderMapper())->addReview(
                    $getData['order'],
                    $getData['article'],
                    substr($postData['review'], 0, 65535)           // Server side protection against too long inputs
                );

                return $this->render('review_thanks');
            }
        }

        $this->render('review', [
            'errors' => $errors,
            'csrf' => $csrf->getToken(),
            'actionUrl' => '/review?' . http_build_query($_GET)
        ]);
    }
}