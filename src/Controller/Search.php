<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Article as ArticleMapper;

class Search extends AbstractController
{
    protected $title = 'Suchen';

    public function run()
    {
        $data = filter_input_array(INPUT_GET, [
            'q' => [
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_SCALAR,
            ],
        ]);

        if (!$data['q']) {
            return $this->notFound();
        }

        $this->render('search', [
            'results' => (new ArticleMapper())->searchByQuery($data['q']),
        ]);
    }
}