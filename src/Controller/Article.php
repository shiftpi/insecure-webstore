<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Article as Mapper;

class Article extends AbstractController
{
    public function run()
    {
        if (!isset($_GET['id'])) {
            return $this->notFound();
        }

        // SQL Injection possible
        $article = (new Mapper())->findById($_GET['id']);

        if (!$article) {
            return $this->notFound();
        }

        $this->title = $article->getTitle();

        return $this->render('article', [
            'article' => $article,
        ]);
    }
}