<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Tool\Auth;
use InsecureWebstore\Validator\CsrfToken;

class Login extends AbstractController
{
    protected $title = 'Anmelden';

    public function run()
    {
        $errors = [];

        $csrf = new CsrfToken();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = filter_input_array(INPUT_POST, [
                'email' => [
                    'filter' => FILTER_VALIDATE_EMAIL,
                    'flags' => FILTER_REQUIRE_SCALAR,
                ],
                'password' => [
                    'flags' => FILTER_REQUIRE_SCALAR,
                ],
                'csrf' => [
                    'filter' => FILTER_CALLBACK,
                    'options' => [
                        $csrf, 'isValid',
                    ],
                ],
            ]);

            foreach ($data as $field => $value) {
                if (!$value) {
                    $errors[$field] = true;
                }
            }

            if (count($errors) === 0) {
                if (Auth::getInstance()->authenticate($data['email'], $data['password'])) {
                    // Open redirect vulnerability
                    $redirectUrl = $_GET['returnUrl'] ?? '/';

                    http_response_code(302);
                    header('Location: ' . $redirectUrl);
                    return;
                } else {
                    // Authentication failed
                    $errors['auth'] = true;
                }
            }
        }

        $this->render('login', [
            'errors' => $errors,
            'csrf' => $csrf->getToken(),
            'actionUrl' => '/login?' . http_build_query($_GET)
        ]);
    }
}