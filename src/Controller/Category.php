<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Article as ArticleMapper;
use InsecureWebstore\Mapper\Category as CategoryMapper;

class Category extends AbstractController
{
    public function run()
    {
        if (!isset($_GET['id'])) {
            return $this->notFound();
        }

        // Unfiltered, but protected by casting and in mapper
        $category = (new CategoryMapper())->findById((int) $_GET['id']);

        if (!$category) {
            return $this->notFound();
        }

        $this->title = $category->getTitle();

        $articles = (new ArticleMapper())->findByCategory((int) $_GET['id']);

        return $this->render('category', [
            'articles' => $articles,
        ]);
    }
}