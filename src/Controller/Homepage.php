<?php
declare(strict_types = 1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Article;

class Homepage extends AbstractController
{
    protected $title = 'Sport Shop';

    public function run()
    {
        return $this->render('homepage', [
            'articles' => (new Article())->findRandom(3)
        ]);
    }
}