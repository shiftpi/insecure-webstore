<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

class NotFound extends AbstractController
{
    protected $title = 'Seite nicht gefunden';

    public function run()
    {
        $this->render('notfound');
    }
}