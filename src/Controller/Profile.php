<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Order as OrderMapper;
use InsecureWebstore\Tool\Auth;

class Profile extends AbstractController
{
    protected $title = 'Dein Profil';

    public function run()
    {
        if (!$user = Auth::getInstance()->getUser()) {
            return $this->notFound();
        }

        return $this->render('profile', [
            'user' => $user,
            'orders' => (new OrderMapper())->findByCustomer($user),
        ]);
    }
}