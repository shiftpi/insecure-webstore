<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Validator\Iban;

class Validator extends AbstractController
{
    public function run()
    {
        if (!isset($_GET['type']) || !isset($_GET['value'])) {
            return $this->notFound();
        } else if ($_GET['type'] === 'iban') {
            $result = !!(new Iban())->isValid($_GET['value']);
        } else {
            return $this->notFound();
        }

        header('Content-Type: application/json');
        echo json_encode(['valid' => $result]);
    }
}