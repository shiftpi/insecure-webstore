<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Tool\Auth;

class Logout extends AbstractController
{
    public function run()
    {
        // CSRF attacks possible
        Auth::getInstance()->unAuthenticate();

        http_response_code(302);
        header('Location: /');
    }
}