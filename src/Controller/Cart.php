<?php
declare(strict_types=1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Article as ArticleMapper;
use InsecureWebstore\Model\Cart as CartModel;

class Cart extends AbstractController
{
    protected $title = 'Warenkorb';

    private function add(int $articleId)
    {
        $article = (new ArticleMapper())->findById($articleId);

        if (!$article) {
            return $this->notFound();
        }

        CartModel::getInstance()->addArticle((int) $_POST['amount'], $article);

        $this->redirectToGet();
    }

    private function remove(int $articleId)
    {
        CartModel::getInstance()->removeArticle($articleId);

        $this->redirectToGet();
    }

    private function redirectToGet()
    {
        http_response_code(302);
        header('Location: /cart');
    }

    public function run()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['add'])) {
            return $this->add((int) $_GET['add']);
        } else if (isset($_GET['remove'])) {                                            // CSRF possible
            return $this->remove((int) $_GET['remove']);
        }

        $cart = CartModel::getInstance()->getArticles();
        $totalCost = 0;

        foreach ($cart as $cartEntry) {
            $totalCost += $cartEntry->getCost();
        }

        return $this->render('cart', [
            'cart' => $cart,
            'totalCost' => $totalCost,
        ]);
    }
}