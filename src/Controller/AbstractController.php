<?php
declare(strict_types = 1);

namespace InsecureWebstore\Controller;

use InsecureWebstore\Mapper\Category as CategoryMapper;
use InsecureWebstore\Tool\Auth;

abstract class AbstractController
{
    protected $title = '';

    abstract public function run();

    protected function render(string $template, array $variables = [])
    {
        $templatePath = 'view/' . $template . '.phtml';

        extract($variables);                                                    // Make variables from controller visible to view
        $title = $this->title;

        ob_start();
        require $templatePath;
        $content = ob_get_clean();                                              // Get view's content and inject it into the layout

        $categories = (new CategoryMapper())->findAll();                        // Grab the categories for the nav bar
        $user = Auth::getInstance()->getUser();                                 // and do the same for the user

        $reflector = new \ReflectionClass(get_class($this));
        $classPath = str_replace(getcwd(), '', $reflector->getFileName());
        $sourceLink = 'https://bitbucket.org/shiftpi/insecure-webstore/src/master' . $classPath;

        require 'view/_layout.phtml';
    }

    protected function notFound()
    {
        (new NotFound())->run();
    }
}