<?php
declare(strict_types=1);

namespace InsecureWebstore\Mapper;

use InsecureWebstore\Model\CartEntry;
use InsecureWebstore\Model\Order as OrderModel;
use InsecureWebstore\Model\Article as ArticleModel;
use InsecureWebstore\Model\Customer as CustomerModel;
use InsecureWebstore\Tool\DbConnection;

class Order
{
    public function findByCustomer(CustomerModel $customer): array
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare(
            'SELECT o.id AS order_id, o.created, o.iban, o.comment, ao.amount, ao.review, a.id AS article_id,'
                        . ' a.price, a.title FROM `order` o'
                            . ' INNER JOIN article_order ao ON o.id = ao.order_id'
                            . ' INNER JOIN article a ON ao.article_id = a.id'
                        . ' WHERE o.customer_id = :customerId'
                        . ' ORDER BY o.created DESC'
        );


        $stmt->bindValue(':customerId', $customer->getId(), \PDO::PARAM_INT);
        $stmt->execute();

        $orderedItems = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        /** @var OrderModel[] $orders */
        $orders = [];

        foreach ($orderedItems as $orderedItem) {
            $orderId = (int) $orderedItem['order_id'];

            if (!isset($orders[$orderId])) {
                $orders[$orderId] = new OrderModel($orderId, $orderedItem['created'], $orderedItem['iban'],
                    $orderedItem['comment'], $customer->getId());
            }

            $article = new ArticleModel((int) $orderedItem['article_id'], $orderedItem['title'], null,
                (float) $orderedItem['price']);
            $cartEntry = new CartEntry((int) $orderedItem['amount'], $article, $orderedItem['review']);

            $orders[$orderId]->addCartEntry($cartEntry);
        }

        return $orders;
    }

    public function save(OrderModel $order)
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare(
            'INSERT INTO `order` (created, iban, comment, customer_id) VALUES (CURRENT_TIMESTAMP, :iban, :comment, :customer_id)'
        );

        $stmt->bindValue(':iban', $order->getIban(), \PDO::PARAM_STR);
        $stmt->bindValue(':comment', $order->getComment(), \PDO::PARAM_STR);
        $stmt->bindValue(':customer_id', $order->getCustomerId(), \PDO::PARAM_INT);

        $stmt->execute();

        $order->setId((int) DbConnection::getInstance()->getConnection()->lastInsertId());

        foreach ($order->getCartEntries() as $cartEntry) {
            $stmt = DbConnection::getInstance()->getConnection()->prepare(
                'INSERT INTO article_order (article_id, order_id, amount) VALUES (:article_id, :order_id, :amount)'
            );

            $stmt->bindValue(':article_id', $cartEntry->getArticle()->getId(), \PDO::PARAM_INT);
            $stmt->bindValue(':order_id', $order->getId(), \PDO::PARAM_INT);
            $stmt->bindValue(':amount', $cartEntry->getAmount(), \PDO::PARAM_INT);

            $stmt->execute();
        }
    }

    public function addReview(int $orderId, int $articleId, string $review)
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare(
            'UPDATE article_order SET review = :review  WHERE article_id = :articleId AND order_id = :orderId AND review IS NULL;'
        );

        $stmt->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $stmt->bindValue(':orderId', $orderId, \PDO::PARAM_INT);
        $stmt->bindValue(':review', $review, \PDO::PARAM_STR);

        $stmt->execute();
    }
}