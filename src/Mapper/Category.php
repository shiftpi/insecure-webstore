<?php
declare(strict_types=1);

namespace InsecureWebstore\Mapper;

use InsecureWebstore\Tool\DbConnection;
use InsecureWebstore\Model\Category as Model;

class Category
{
    public function findById(int $id): ?Model
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare('SELECT * FROM category WHERE id = :id');
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);

        $stmt->execute();
        $category = $stmt->fetchObject(Model::class);

        return $category ? $category : null;
    }

    public function findAll(): \Traversable
    {
        $connection = DbConnection::getInstance()->getConnection();

        return $connection->query('SELECT * FROM category ORDER BY title', \PDO::FETCH_CLASS, Model::class);
    }
}