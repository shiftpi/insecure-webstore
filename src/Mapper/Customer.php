<?php
declare(strict_types=1);

namespace InsecureWebstore\Mapper;

use InsecureWebstore\Tool\DbConnection;
use InsecureWebstore\Model\Customer as CustomerModel;

class Customer
{
    public function findByEmailPassword(string $email, string $password): ?CustomerModel
    {
        // Insecure password hashing
        $stmt = DbConnection::getInstance()->getConnection()->prepare('SELECT * FROM customer WHERE email LIKE :email AND password LIKE MD5(:password)');

        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':password', $password);
        $stmt->execute();

        $customer = $stmt->fetchObject(CustomerModel::class);

        return $customer ? $customer : null;
    }
}