<?php
declare(strict_types=1);

namespace InsecureWebstore\Mapper;

use InsecureWebstore\Tool\DbConnection;
use InsecureWebstore\Model\Article as Model;

class Article
{
    public function findByCategory(int $categoryId): array
    {
        $connection = DbConnection::getInstance()->getConnection();

        // SQL Injection
        return $connection->query('SELECT * FROM article a INNER JOIN article_category c'
        . ' ON a.id = c.article_id WHERE category_id = ' . $categoryId . ' ORDER BY title')
            ->fetchAll(\PDO::FETCH_CLASS, Model::class);
    }

    public function findRandom(int $count): array
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare('SELECT * FROM article ORDER BY RAND() LIMIT :limit');
        $stmt->bindValue(':limit', $count, \PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, Model::class);
    }

    // Don't use type declaration for $categoryId at this point, due to keep the SQL injection vulnerability open
    // public function findById(int $id): ?Model
    public function findById($id): ?Model
    {
        $stmt = DbConnection::getInstance()->getConnection()->prepare(
            'SELECT a.*, o.review FROM article a LEFT JOIN article_order o on a.id = o.article_id WHERE a.id = ' . $id
        );

        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($rows) === 0) {
            return null;
        }

        $article = Model::factory($rows[0]);
        $article->setReviews([]);

        foreach ($rows as $row) {
            if ($row['review']) {
                $article->addReview($row['review']);
            }
        }

        return $article;
    }

    public function searchByQuery(string $query): array
    {
        $query = '%' . $query . '%';

        $stmt = DbConnection::getInstance()->getConnection()->prepare('SELECT * FROM articel WHERE title LIKE :query OR description LIKE :query');
        $stmt->bindValue(':query', $query, \PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, Model::class);
    }
}